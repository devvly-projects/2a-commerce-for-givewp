<?php

use GiveFluidPay\HelperTrait;

class Test_FluidPayHelperTrait extends WP_UnitTestCase {
  use HelperTrait;

  /**
   * @covers \GiveFluidPay\HelperTrait::getBillingFrequency()
   * @return void
   */
  public function test_getBillingFrequency() {
    $this->assertEquals('monthly', $this->getBillingFrequency('month'));
    $this->assertEquals('daily', $this->getBillingFrequency());
  }

  /**
   * @covers \GiveFluidPay\HelperTrait::getBillingDays()
   * @return void
   */
  public function test_getBillingDays() {
    $this->assertEquals('4', $this->getBillingDays(4, 'daily'));
    $this->assertEquals('0', $this->getBillingDays(31, 'monthly'));
    $this->assertEquals(null, $this->getBillingDays(-1, 'whatever'));
  }
}