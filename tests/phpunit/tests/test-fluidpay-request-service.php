<?php

use GiveFluidPay\Services\RequestService;
use GuzzleHttp\Exception\GuzzleException;

class Test_FluidPayRequestService extends WP_UnitTestCase {

  use \GiveFluidPay\HelperTrait;

  private string $api_key;
  private string $api_url;
  private RequestService $requestService;

  /**
   * @covers \GiveFluidPay\Services\RequestService::getAPIKeys()
   * @return void
   * @throws JsonException
   * @throws GuzzleException
   */
  public function test_connection() {
    $response = $this->requestService->getAPIKeys();
    $this->assertEquals( 'success', $response['status']);
    $this->assertArrayHasKey('data', $response);
    $this->assertNotEmpty($response['data']);
  }

  /**
   * @covers \GiveFluidPay\Services\RequestService::createCustomer()
   * @return void
   * @throws JsonException
   * @throws GuzzleException
   */
  public function test_createCustomer() {
    $customer = $this->getTypicalCustomer();

    $response = $this->requestService->createCustomer($customer);
    $this->assertEquals( 'success', $response['status']);
    $this->assertArrayHasKey('data', $response);
    $this->assertNotEmpty($response['data']);
  }

  /**
   * @covers \GiveFluidPay\Services\RequestService::getAllPlans()
   * @return void
   * @throws JsonException
   * @throws GuzzleException
   */
  public function test_getAllPlans() {
    $customer = $this->getTypicalCustomer();

    $response = $this->requestService->getAllPlans();
    $this->assertEquals( 'success', $response['status']);
    $this->assertArrayHasKey('data', $response);
    $this->assertNotEmpty($response['data']);
  }

  /**
   * @covers \GiveFluidPay\Services\RequestService::processTransaction()
   * @return void
   * @throws JsonException
   * @throws GuzzleException
   */
  function test_ProcessTransaction() {

    $transaction = [
      "type" => "sale",
      "order_id" => 5416848,
      "amount" => 3000,
      "tax_amount" => 0,
      "shipping_amount" => 0,
      "currency" => "USD",
      "description" => "test transaction",
      "ip_address" => "192.168.1.1",
      "email_receipt" => FALSE,
      "email_address" => 'test@mail.com',
      "create_vault_record" => TRUE,
      "payment_method" => [
        "card" => array(
          "entry_type" => "keyed",
          "number" => "4111111111111111",
          "expiration_date" => "12/22",
          "cvc" => "999"
        )
      ],
      "billing_address" => [
        "first_name" => 'CustomerName',
        "last_name" => 'CustomerSurname',
        "company" => '2adata',
        "address_line_1" => '22 Public Square STE 11',
        "address_line_2" => "",
        "city" => 'Columbia',
        "state" => 'Tennessee',
        "postal_code" => '38401',
        "country" => "US",
        "email" => 'test@mail.com'
      ]];

    $response = $this->requestService->processTransaction($transaction);
    $this->assertEquals( 'success', $response['status']);
  }

  /**
   * @covers \GiveFluidPay\Services\RequestService::createPlan()
   * @return void
   * @throws JsonException
   * @throws GuzzleException
   */
  public function test_createPlan(){
    $plan = $this->getTypicalPlanData();

    $response = $this->requestService->createPlan($plan);
    $this->assertEquals( 'success', $response['status']);
  }

  /**
   * @covers \GiveFluidPay\Services\RequestService::createSubscription()
   * @return void
   * @throws JsonException
   * @throws GuzzleException
   */
  public function test_createSubscription() {
    $subscription = $this->getTypicalSubscriptionData();

    $response = $this->requestService->createSubscription($subscription);
    $this->assertEquals( 'success', $response['status']);
  }


  public function set_up() {
    $this->api_url = $_ENV['TEST_API_URL'];
    $this->api_key = $_ENV['TEST_API_KEY'];

    $this->requestService = new RequestService(
      $this->api_url, $this->api_key
    );
  }

  /**
   * @return array
   */
  private function getTypicalCustomer(): array
  {
    return [
      'id_format' => 'xid_type_last4',
      'description' => 'test description',
      'flags' => ['surcharge_exempt'],
      'default_payment' => [
        'card' => [
          'number' => '4111111111111111',
          'expiration_date' => '12/' . date('Y', strtotime('now +1 year')),
          'cvc' => '987'
        ]
      ],
      'default_billing_address' => [
        'first_name' => '',
        'last_name' => 'Kabalan',
        'company' => '2adata',
        'address_line_1' => '22 Public Square STE 11',
        'address_line_2' => '',
        'city' => 'Columbia',
        'state' => 'Tennessee',
        'postal_code' => '38401',
        'country' => 'US',
        'email' => 'test@mail.com'
      ],
      'default_shipping_address' => [
        'first_name' => 'CustomerName',
        'last_name' => 'CustomerSurname',
        'company' => '2adata',
        'address_line_1' => '22 Public Square STE 11',
        'address_line_2' => '',
        'city' => 'Columbia',
        'state' => 'Tennessee',
        'postal_code' => '38401',
        'country' => 'US',
        'email' => 'test@mail.com']
    ];
  }

  private function getTypicalRecurringPaymentData(): array
  {
    return array (
      'price' => 25.0,
      'purchase_key' => '27d3c520b84e4a6d8333ad1b3da9552c',
      'user_email' => 'test@mail.com',
      'date' => '2022-10-12 07:28:09',
      'user_info' =>
        array (
          'id' => 1,
          'title' => '',
          'email' => 'test@mail.com',
          'first_name' => 'CustomerName',
          'last_name' => 'Test',
          'address' =>
            array (
              'line1' => '3777 Nolensville Pk',
              'line2' => false,
              'city' => 'Nashville',
              'state' => 'TN',
              'zip' => '37211',
              'country' => 'US',
            ),
        ),
      'post_data' =>
        array (
          'give-honeypot' => '',
          'give-form-id-prefix' => '277837-1',
          'give-form-id' => '277837',
          'give-form-title' => 'Donate',
          'give-current-url' => 'https://goa-givewp-fluidpay.ddev.site/',
          'give-form-url' => 'https://goa-givewp-fluidpay.ddev.site/',
          'give-form-minimum' => '1.00',
          'give-form-maximum' => '999999.99',
          'give-form-hash' => '54a969350b',
          'give-price-id' => '0',
          'give-recurring-logged-in-only' => '',
          'give-logged-in-only' => '1',
          '_give_is_donation_recurring' => '1',
          'give_recurring_donation_details' => '{"give_recurring_option":"yes_donor"}',
          'give-amount' => '25.00',
          'give-radio-donation-level' => '25.00',
          'give-recurring-period' => 'on',
          'payment-mode' => 'fluidpay',
          'give_first' => 'CustomerName',
          'give_last' => 'Test',
          'give_email' => 'test@mail.com',
          'phone_number' => '',
          'card_number' => '4111 1111 1111 1111',
          'card_cvc' => '200',
          'card_name' => 'CustomerName',
          'card_exp_month' => '12',
          'card_exp_year' => '2024',
          'card_expiry' => '12 / 24',
          'billing_country' => 'US',
          'card_address' => '3777 Nolensville Pk',
          'card_address_2' => '',
          'card_city' => 'Nashville',
          'card_state' => 'TN',
          'card_zip' => '37211',
          'give-user-id' => '1',
          'give_action' => 'purchase',
          'give-gateway' => 'fluidpay',
        ),
      'gateway' => 'fluidpay',
      'card_info' =>
        array (
          'card_name' => 'CustomerName',
          'card_number' => '4111111111111111',
          'card_cvc' => '200',
          'card_exp_month' => '12',
          'card_exp_year' => '2024',
          'card_address' => '3777 Nolensville Pk',
          'card_address_2' => '',
          'card_city' => 'Nashville',
          'card_state' => 'TN',
          'card_country' => 'US',
          'card_zip' => '37211',
        ),
      'period' => 'month',
      'times' => '0',
      'frequency' => '1',
      'gateway_nonce' => 'ba3a9cde80',
    );
  }

  private function getTypicalPlanData() {
    return array (
      'name' => 'Donate',
      'description' => 'Plan for recurring donation',
      'amount' => 2500.0,
      'billing_cycle_interval' => 1,
      'billing_frequency' => 'monthly',
      'billing_days' => '0',
      'duration' => 0,
      'add_ons' =>
        array (
        ),
      'discounts' =>
        array (
        ),
    );
  }

  private function getTypicalSubscriptionData(): array
  {
    return array (
      'plan_id' => 'cd3ae3s6lr8rkbkkl0cg',
      'description' => 'Subscription description',
      'customer' =>
        array (
          'id' => 'cd3aj846lr8rkbkkl1jg-card-1881',
        ),
      'amount' => 2500.0,
      'billing_cycle_interval' => 1,
      'billing_frequency' => 'monthly',
      'billing_days' => '0',
      'duration' => 0,
      'next_bill_date' => '2022-11-12',
      'add_ons' =>
        array (
        ),
      'discounts' =>
        array (
        ),
      'test_field' => 'qwqwqqwq',
      'custom_fields' =>
        array (
          'cd3aj846lr8rkbkkl1jg-card-1881' =>
            array (
              0 => 'test_field',
            ),
        ),
      'payment_method' =>
        array (
          'customer' =>
            array (
              'id' => 'cd3aj846lr8rkbkkl1jg-card-1881',
              'payment_method_type' => 'card',
              'payment_method_id' => 'cd3aj846lr8rkbkkl1j0',
              'billing_address_id' => 'cd3aj846lr8rkbkkl1i0',
              'shipping_address_id' => 'cd3aj846lr8rkbkkl1i0',
              'custom_fields' =>
                array (
                  'cd3aj846lr8rkbkkl1jg-card-1881' =>
                    array (
                      0 => 'test_field',
                    ),
                ),
            ),
        ),
    );
  }

}