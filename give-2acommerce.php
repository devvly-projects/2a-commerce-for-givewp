<?php
/**
 * Plugin Name:      Give - FluidPay Gateway
 * Plugin URI:          https://givewp.com/addons/fluidpay-gateway/
 * Description:          Adds the fluidpay.com payment gateway to the available GiveWP payment methods.
 * Version:              1.0.0
 * Requires at least:   4.9
 * Requires PHP:        5.6
 * Author:              GiveWP
 * Author URI:          https://givewp.com/
 * Text Domain:          give-fluidpay
 * Domain Path:          /languages
 *
 * @package             Give
 * @subpackage          FluidPay Premium
 */
// Exit if accessed directly.


use GiveFluidPay\Webhooks\Listeners\PaymentCaptureCreated;

if (!defined('ABSPATH')) {
  exit;
}
/**
 * Define constants.
 * Required minimum versions, paths, urls, etc.
 */
if (!defined('GIVE_FLUIDPAY_VERSION')) {
  define('GIVE_FLUIDPAY_VERSION', '1.0.0');
}
if (!defined('GIVE_FLUIDPAY_MIN_GIVE_VER')) {
  define('GIVE_FLUIDPAY_MIN_GIVE_VER', '2.16.0');
}
if (!defined('GIVE_FLUIDPAY_PLUGIN_FILE')) {
  define('GIVE_FLUIDPAY_PLUGIN_FILE', __FILE__);
}
if (!defined('GIVE_FLUIDPAY_PLUGIN_DIR')) {
  define('GIVE_FLUIDPAY_PLUGIN_DIR', dirname(GIVE_FLUIDPAY_PLUGIN_FILE));
}
if (!defined('GIVE_FLUIDPAY_PLUGIN_URL')) {
  define('GIVE_FLUIDPAY_PLUGIN_URL', plugin_dir_url(GIVE_FLUIDPAY_PLUGIN_FILE));
}
if (!defined('GIVE_FLUIDPAY_BASENAME')) {
  define('GIVE_FLUIDPAY_BASENAME', plugin_basename(GIVE_FLUIDPAY_PLUGIN_FILE));
}
define('WPSP_NAME','fluidpay');
define( "WPSP_URL", trailingslashit( plugin_dir_url( __FILE__ ) ) );

require_once GIVE_FLUIDPAY_PLUGIN_DIR . '/vendor/autoload.php';

  /**
   * Class GiveFluidPay.
   */

if ( ! class_exists( 'Give_FluidPay_Premium' ) ) {

  class Give_FluidPay_Premium
  {

    private static ?Give_FluidPay_Premium $instance = null;

    public $upgrades;
    /**
     * Notices (array)
     *
     * @var array
     */
    public array $notices = [];

    private array $serviceProviders = [
      \GiveFluidPay\Donations\ServiceProviders\ServiceProvider::class
    ];

    private \GiveFluidPay\Services\RequestService $requestService;

    public static function get_instance(): Give_FluidPay_Premium
    {
      if (null === self::$instance) {
        self::$instance = new self();
        self::$instance->setup();
      }
      return self::$instance;
    }

    /**
     * Setup Give FluidPay.
     *
     * @since  2.1.1
     * @access private
     */
    private function setup()
    {
      // Give init hook.
      add_action('give_init', [$this, 'init'], 10);
      add_action('before_give_init', [$this, 'registerServiceProvider']);
    }

    /**
     * Init the plugin after plugins_loaded so environment variables are set.
     *
     * @since 2.1.1
     */
    public function init()
    {
      add_filter('give_payment_gateways', [$this, 'register_gateway']);
      add_filter('give_get_sections_gateways', [$this, 'fluidpay_for_give_register_payment_gateway_sections']);
      add_filter('give_get_settings_gateways', [$this, 'fluidpay_for_give_register_payment_gateway_setting_fields']);
      add_action('give_gateway_fluidpay', [$this, 'fluidpay_for_give_process_fluidpay_donation']);
      add_filter('give_recurring_available_gateways', [$this, 'give_recurring_fluidpay_gateway_setup']);
      add_filter('plugin_action_links', [$this, 'give_fluidpay_plugin_action_links']);
      $api_url = give_get_option('option_fluidpay_for_give_fluidpay_api_url');
      $api_key = give_get_option('option_fluidpay_for_give_fluidpay_api_key');
      $this->requestService = new \GiveFluidPay\Services\RequestService($api_url, $api_key);

    }

    public function give_fluidpay_plugin_action_links( $actions ): array
    {
      $new_actions = array(
        'settings' => sprintf(
          '<a href="%1$s">%2$s</a>',
          admin_url( 'edit.php?post_type=give_forms&page=give-settings&tab=gateways&section=fluidpay-settings' ),
          esc_html__( 'Settings', 'give-fluidpay' )
        ),
      );

      return array_merge( $new_actions, $actions );
    }

    /**
     * Register the FluidPay payment gateways.
     *
     * @access public
     * @param array $gateways List of registered gateways.
     * @return array
     * @since  1.0
     */
    public function register_gateway(array $gateways): array
    {
      $gateways['fluidpay'] = ['admin_label' => __('FluidPay - Credit Card', 'fluidpay-for-give'),
        // This label will be displayed under Give settings in admin.
        'checkout_label' => __('Credit Card', 'fluidpay-for-give'),
        // This label will be displayed on donation form in frontend.
      ];
      return $gateways;
    }

    /**
     * Register Section for Payment Gateway Settings.
     *
     * @param array $sections List of payment gateway sections.
     * @return array
     * @since 1.0.0
     */
    public function fluidpay_for_give_register_payment_gateway_sections(array $sections): array
    {
      // `fluidpay-settings` is the name/slug of the payment gateway section.
      $sections['fluidpay-settings'] = __('FluidPay', 'fluidpay-for-give');
      return $sections;
    }

    /**
     * Register Admin Settings.
     *
     * @param array $settings List of admin settings.
     * @return array
     * @since 1.0.0
     */
    public function fluidpay_for_give_register_payment_gateway_setting_fields(array $settings): array
    {
      if (give_get_current_setting_section() == 'fluidpay-settings') {
        $settings = [
          ['id' => 'give_title_fluidpay',
            'type' => 'title',
          ]
        ];
        $settings[] = [
          'name' => __('Webhook Notification', 'give-webhook-notification'),
          'desc' => __("In order to receive Recurring Donation notifications you need to add the website Webhook in your payment gateway as following: https://www.your-wordpress-domain/?give_up_transaction=1.", 'fluidpay-for-give-webhook-notification'),
          'id' => 'option_fluidpay_for_give_fluidpay_webhook_notification',
          'type' => 'title'
        ];
        $settings[] = [
          'name' => __('API Key', 'give-square'),
          'desc' => __('Enter your API Key, found in your FluidPay Dashboard.', 'fluidpay-for-give'),
          'id' => 'option_fluidpay_for_give_fluidpay_api_key',
          'type' => 'text'
        ];
        $settings[] = [
          'name' => __('API URL', 'give-square'),
          'desc' => __('Enter your API Url, found in your FluidPay Dashboard.', 'fluidpay-for-give'),
          'id' => 'option_fluidpay_for_give_fluidpay_api_url',
          'type' => 'text'
        ];
        $settings[] = ['id' => 'give_title_fluidpay',
          'type' => 'sectionend',];
      } // End switch().
      // add if condition write values into a file , to use them in unit test
      return $settings;
    }

    /**
     * Process Square checkout submission.
     *
     * @param array $posted_data List of posted data.
     * @return void
     * @since  1.0.0
     * @access public
     */
    public function fluidpay_for_give_process_fluidpay_donation(array $posted_data)
    {
      // Make sure we don't have any left over errors present.
      give_clear_errors();
      // Any errors?
      $errors = give_get_errors();
      // No errors, proceed.
      if (!$errors) {
        $form_id = intval($posted_data['post_data']['give-form-id']);
        $price_id = !empty($posted_data['post_data']['give-price-id']) ? $posted_data['post_data']['give-price-id'] : 0;
        $donation_amount = !empty($posted_data['price']) ? $posted_data['price'] : 0;
        // Setup the payment details.
        $donation_data = ['price' => $donation_amount,
          'give_form_title' => $posted_data['post_data']['give-form-title'],
          'give_form_id' => $form_id,
          'give_price_id' => $price_id,
          'date' => $posted_data['date'],
          'user_email' => $posted_data['user_email'],
          'purchase_key' => $posted_data['purchase_key'],
          'currency' => give_get_currency($form_id),
          'user_info' => $posted_data['user_info'],
          'status' => 'pending',
          'gateway' => 'fluidpay',];
        // Record the pending donation.
        $donation_id = give_insert_payment($donation_data);
        if (!$donation_id) {
          // Record Gateway Error as Pending Donation in Give is not created.
          give_record_gateway_error(__('FluidPay Error', 'fluidpay-for-give'), __('Unable to create a pending donation with Give.', 'fluidpay-for-give'));
          // Send user back to checkout.
          give_send_back_to_checkout('?payment-mode=fluidpay');
          return;
        }
        // Do the actual payment processing using the custom payment gateway API. To access the GiveWP settings, use give_get_option()
        // as a reference, this pulls the API key entered above: give_get_option('fluidpay_for_give_fluidpay_api_key')
        //create a customer for 2agateway
        $customer = array(
          "id_format" => "xid_type_last4",
          "description" => "test description",
          "flags" => ["surcharge_exempt"],
          "default_payment" => array(
            "card" => array(
              "number" => $posted_data['card_info']['card_number'],
              "expiration_date" => $posted_data['card_info']['card_exp_month'] . '/' . substr($posted_data['card_info']['card_exp_year'], -2),
              "cvc" => $posted_data['card_info']['card_cvc']
            )
          ),

          "default_billing_address" => array(
            "first_name" => $donation_data['user_info']['first_name'],
            "last_name" => $donation_data['user_info']['last_name'],
            "company" => '2adata',
            "address_line_1" => $donation_data['user_info']['address']['line1'],
            "address_line_2" => "",
            "city" => $donation_data['user_info']['address']['city'],
            "state" => $donation_data['user_info']['address']['state'],
            "postal_code" => $donation_data['user_info']['address']['zip'],
            "country" => "US",
            "email" => $donation_data['user_email']
          ),
          "default_shipping_address" => array(
            "first_name" => $donation_data['user_info']['first_name'],
            "last_name" => $donation_data['user_info']['last_name'],
            "company" => '2adata',
            "address_line_1" => $donation_data['user_info']['address']['line1'],
            "address_line_2" => "",
            "city" => $donation_data['user_info']['address']['city'],
            "state" => $donation_data['user_info']['address']['state'],
            "postal_code" => $donation_data['user_info']['address']['zip'],
            "country" => "US",
            "email" => $donation_data['user_email']
          )
        );

        $customer_response = $this->requestService->createCustomer($customer);
        if ($customer_response['status'] == 'success') {
          $transaction_data = [
            "type" => "sale",
            "order_id" => $donation_id,
            "amount" => $donation_data['price'] * 100,
            "tax_amount" => 0,
            "shipping_amount" => 0,
            "currency" => "USD",
            "ip_address" => $_SERVER["HTTP_CF_CONNECTING_IP"] ?? $this->getRealIpAddr(),
            "email_receipt" => TRUE,
            "email_address" => $donation_data['user_email'],
            "create_vault_record" => FALSE,
            "payment_method" => [
              "customer" => array(
                "id" => $customer_response['data']['id'],
                "payment_method_type" => "card",
                "payment_method_id" => $customer_response['data']['data']['customer']['defaults']['payment_method_id'],
                "billing_address_id" => $customer_response['data']['data']['customer']['defaults']['billing_address_id'],
                "shipping_address_id" => $customer_response['data']['data']['customer']['defaults']['shipping_address_id']
              )
            ],
            "billing_address" => [
              "first_name" => $donation_data['user_info']['first_name'],
              "last_name" => $donation_data['user_info']['last_name'],
              "address_line_1" => $donation_data['user_info']['address']['line1'],
              "address_line_2" => "",
              "city" => $donation_data['user_info']['address']['city'],
              "state" => $donation_data['user_info']['address']['state'],
              "postal_code" => $donation_data['user_info']['address']['zip'],
              "country" => "US",
              "email" => $donation_data['user_email']
            ]];
          $transaction = $this->requestService->processTransaction($transaction_data);
          if ($transaction['status'] == 'success') {
            give_update_payment_status($donation_id, 'complete');
            give_set_payment_transaction_id($donation_id, $transaction['data']['id']);
            if ($transaction['data']['status'] == "pending_settlement") {
              wp_redirect(give_get_success_page_uri());
            }
          } else {
            give_set_error('FluidPay Error',
              __('Not able to create a Transaction on FluidPay gateway', 'give-recurring'));
            give_send_back_to_checkout('?payment-mode=fluidpay');
          }
        } else {
          give_set_error('FluidPay Error',
            __('Not able to create a Customer on FluidPay gateway , please contact our support team', 'give-recurring'));
          give_send_back_to_checkout('?payment-mode=fluidpay');
        }
      } else {
        // Send user back to checkout.
        give_send_back_to_checkout('?payment-mode=fluidpay');
      } // End if().
    }

    public function give_recurring_fluidpay_includes()
    {
      // Bailout, if moneris payment methods is inactive.
      $settings = give_get_settings();
      $gateways = $settings['gateways'] ?? [];
      $active = FALSE;
      if (array_key_exists('fluidpay', $gateways)) {
        if ($gateways['fluidpay'] == "1") {
          $active = TRUE;
        }
      }
      if (!$active) {
        return;
      }
      require_once plugin_dir_path(__FILE__) . 'includes/gateways/give-recurring-fluidpay.php';
    }

    public function give_recurring_fluidpay_gateway_setup($classes)
    {
      $classes['fluidpay'] = 'Give_Recurring_FluidPay';
      $this->give_recurring_fluidpay_includes();
      return $classes;
    }

    public function getRealIpAddr()
    {
      if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
      {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
      } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
      {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } else {
        $ip = $_SERVER['REMOTE_ADDR'];
      }
      return $ip;
    }

    public function webhook_init_internal()
    {
      //used to update inventory
      if (isset($_GET['give_up_transaction'])) {
        $data = file_get_contents('php://input');
        $subscription_hook = new PaymentCaptureCreated();
        $subscription_hook->processEvent(json_decode($data, true));

      }
    }

    public function write_log($log)
    {
      if (is_array($log) || is_object($log)) {
        error_log(print_r($log, true));
      } else {
        error_log($log);
      }
    }


    public function registerServiceProvider()
    {
      foreach ($this->serviceProviders as $serviceProvider) {
        give()->registerServiceProvider($serviceProvider);
      }
    }
  }

}
$GLOBALS['give_fluidpay'] = Give_FluidPay_Premium::get_instance();
