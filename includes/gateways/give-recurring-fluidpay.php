<?php
// Exit if accessed directly.
use GiveFluidPay\HelperTrait;
use GiveFluidPay\RecurringPaymentTrait;
use GiveFluidPay\Services\RequestService;

if (!defined('ABSPATH')) {
  exit;
}

/**
 * Class Give_Recurring_FluidPay
 */
class Give_Recurring_FluidPay extends Give_Recurring_Gateway
{
  use HelperTrait;
  use RecurringPaymentTrait;

  /**
   * Call Give FluidPay Invoice Class for processing recurring donations.
   *
   * @var $invoice
   */
  public $invoice;
  public string $access_token;
  public string $store_id;
  /**
   * Array of subscriptions.
   *
   * @var array
   */
  public $subscriptions = [];
  private RequestService $requestService;

  public function __construct()
  {
    parent::__construct();
    // Cancellation action.
    add_action( 'give_recurring_cancel_' . $this->id . '_subscription', [ $this, 'cancel' ], 10, 2 );
  }

  /**
   * Get FluidPay Started.
   *
   * @return void
   * @since 1.9.0
   */
  public function init()
  {
    $this->id = 'fluidpay';
    if (defined('GIVE_FLUIDPAY_VERSION') && version_compare(GIVE_FLUIDPAY_VERSION, '1.0.0', '<')) {
      add_action('admin_notices', [$this, 'old_api_upgrade_notice']);
      // No FluidPay SDK. Bounce.
      return;
    }
    // Bailout, if gateway is not active.
    if (!give_is_gateway_active($this->id)) {
      return;
    }
    $this->access_token = give_get_option('give_option_fluidpay_access_token');
    $this->store_id = give_get_option('give_option_fluidpay_store_id');
    $api_url = give_get_option('option_fluidpay_for_give_fluidpay_api_url');
    $api_key = give_get_option('option_fluidpay_for_give_fluidpay_api_key');
    $this->requestService = new RequestService($api_url, $api_key);
    add_action('give_recurring_process_checkout', [$this, 'process_recurring_checkout']);
  }

  /**
   * Upgrade notice.
   * Tells the admin that they need to upgrade the FluidPay gateway.
   *
   * @since  1.9.0
   * @access public
   */
  public function old_api_upgrade_notice()
  {
    $message = __('<strong>Attention:</strong> The Recurring Donations plugin requires the latest version of the FluidPay gateway add-on to process donations properly. Please update to the latest version of FluidPay to resolve this issue. If your license is active you should see the update available in WordPress. Otherwise, you can access the latest version by <a href="https://givewp.com/wp-login.php" target="_blank">logging into your account</a> and visiting <a href="https://givewp.com/my-account/#tab_downloads" target="_blank">your downloads</a> page on the Give website.',
        'give-recurring');
    if (class_exists('Give_Notices')) {
      Give()->notices->register_notice(['id' => 'give-activation-error',
        'type' => 'error',
        'description' => $message,
        'show' => TRUE,]);
    } else {
      $class = 'notice notice-error';
      printf('<div class="%1$s"><p>%2$s</p></div>', $class, $message);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function can_cancel( $ret, $subscription ) {

    if (
      $subscription->gateway === $this->id
      && 'active' === $subscription->status
    ) {
      $ret = true;
    }

    return $ret;
  }

  /**
   * @param $subscription Give_Subscription
   * @param $valid bool
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws JsonException
   */
  public function cancel($subscription, $valid)
  {
    if ( empty( $valid ) || false === $this->can_cancel( false, $subscription ) ) {
      return false;
    }

    $response = $this->requestService->cancelSubscription($subscription->profile_id);
    if('success' != $response['msg']) {
      \Give\Log\Log::error(__('GiveFluidPay Error', 'give-fluidpay'));
      give_set_error( 'FluidPay Error', __( 'An error occurred while cancelling the donation. Please try again.', 'give-recurring' ) );
    }
  }

  /**
   * This function will be used to do all the heavy lifting for processing a donation payment.
   *
   * @param array $donation_data List of donation data.
   * @return void
   * @since  1.0.0
   * @access public
   */
  public function process_recurring_checkout(array $donation_data)
  {
    // Bailout, if the current gateway and the posted gateway mismatched.
    if ($this->id !== $donation_data['gateway']) {
      return;
    }
    // Validate gateway nonce.
    give_validate_nonce($donation_data['gateway_nonce'], 'give-gateway');
    // Make sure we don't have any left over errors present.
    give_clear_errors();
    // Fields validation
    // @see https://developer.fluidpay.com/Documentation/NA/E-Commerce%20Solutions/API/Purchase?lang=php
    if (!in_array($donation_data['period'], ['day', 'week', 'month'])) {
      give_set_error('FluidPay Error',
        __('Invalid recurring period. Valid options: day, week, month.', 'give-recurring'));
    }
    // Any errors?
    $errors = give_get_errors();
    // No errors, proceed.
    if (!$errors) {
      $donation_amount = give_format_amount($donation_data['price']);
      $this->purchase_data = apply_filters('give_recurring_purchase_data', $donation_data, $this);
      // Get billing times.
      $times = !empty($this->purchase_data['times']) ? (int)$this->purchase_data['times'] : 0;
      // Get frequency value.
      $frequency = !empty($this->purchase_data['frequency']) ? (int)$this->purchase_data['frequency'] : 1;
      $payment_data = $this->buildPaymentData($donation_amount, $donation_data);
      // Record the pending payment.
      $this->payment_id = give_insert_payment($payment_data);

      //create a customer for 2agateway
      $customer = $this->buildCustomerData($donation_data);
      $customer_response = $this->requestService->createCustomer($customer);
      $subscription_response = [];
      if ($customer_response['status'] == 'success') {
        $transaction_request = $this->buildRecurringTransactionRequest($customer_response, $donation_data);
        $transaction = $this->requestService->processTransaction($transaction_request);
        if ($transaction['status'] == 'success') {
          if ($transaction['data']['status'] == "success" || $transaction['data']['status'] == "pending_settlement") {
            give_set_payment_transaction_id($this->payment_id, $transaction['data']['id']);
            give_update_payment_status($this->payment_id, 'complete');
            $this->subscriptions = apply_filters('give_recurring_subscription_pre_gateway_args',
              [
                'name' => $this->purchase_data['post_data']['give-form-title'],
                'id' => $this->purchase_data['post_data']['give-form-id'],
                'form_id' => $this->purchase_data['post_data']['give-form-id'],
                'price_id' => $this->get_price_id(),
                'initial_amount' => give_sanitize_amount_for_db($this->purchase_data['price']),
                // add fee here in future.
                'recurring_amount' => give_sanitize_amount_for_db($this->purchase_data['price']),
                'period' => self::get_interval($this->purchase_data['period'], $frequency),
                'frequency' => self::get_interval_count($this->purchase_data['period'], $frequency),
                // Passed interval. Example: charge every 3 weeks.
                'bill_times' => give_recurring_calculate_times($times, $frequency),
                // Profile ID for this subscription - This is set by the payment gateway.
                'transaction_id' => $transaction['data']['id'],// Transaction ID for this subscription - This is set by the payment gateway.
              ],
              $this->purchase_data);
            //create subscription on fluidpay gateway
            $billing_cycle_interval = self::get_interval_count($this->purchase_data['period'], $frequency);
            if ($this->subscriptions['period'] == 'week') {
              $billing_cycle_interval = 7;
            }
            if ($this->subscriptions['period'] == 'day') {
              $billing_cycle_interval = 1;
            }
            $billing_frequency = $this->getBillingFrequency($this->subscriptions['period']);
            $billing_days = $this->getBillingDays($billing_cycle_interval, $billing_frequency);
            $duration = 0;
            // Lookup for all plans and get the matched plan with amount and frequency , then insert plan_id in subscription request .if there is a match
            // if no match create a new plan
            $plans_result = $this->requestService->getAllPlans();
            $gateway_plans = [];
            $plan = [];
            if ($plans_result["status"] == "success") {
              $gateway_plans = $plans_result["data"];
            }
            foreach ($gateway_plans as $key => $gateway_plan) {
              if (
                $gateway_plan['amount'] == $donation_data['price'] * 100 &&
                $gateway_plan['billing_cycle_interval'] == $billing_cycle_interval &&
                $gateway_plan['billing_frequency'] == $billing_frequency
              ) {
                $plan = $gateway_plan;
                break;
              }
            }
            //check if there a match for plan , if not add new plan
            if (!isset($plan['id'])) {
              $donation_price =$donation_data['price'];
              $plan_body = [
                "name" => "Donation-$donation_price",
                "description" => "Plan for recurring donation",
                "amount" => $donation_price,
                "billing_cycle_interval" => $billing_cycle_interval,
                "billing_frequency" => $billing_frequency,
                "billing_days" => $billing_days,
                "duration" => $duration,
                "add_ons" => [],
                "discounts" => []
              ];
              $plan_response = $this->requestService->createPlan($plan_body);
              if ($plan_response["status"] == "success") {
                $plan = $plan_response["data"];
              } else {
                //todo , refund the transaction above
                give_set_error('FluidPay Error',
                  __('Not able to create a plan on FluidPay gateway , please contact our support team, ' . $plan_response["msg"], 'give-recurring'));
                give_send_back_to_checkout('?payment-mode=fluidpay');
              }
            }
            $next_billing_date = $this->getNextBillingDate($billing_cycle_interval, $billing_frequency);
            $subscription_response = [];
            $subscription = [
              "plan_id" => $plan['id'],
              "description" => "Subscription description",
              "customer" => ["id" => $customer_response['data']['id']],
              "amount" => $donation_data['price'] * 100,
              "billing_cycle_interval" => $billing_cycle_interval,
              "billing_frequency" => $billing_frequency,
              "billing_days" => $billing_days,
              "duration" => $duration,
              "next_bill_date" => $next_billing_date,
              "add_ons" => [],
              "discounts" => [],
              "payment_method" =>
                [
                  "customer" => [
                    "id" => $customer_response['data']['id'],
                    "payment_method_type" => "card",
                    "payment_method_id" => $customer_response['data']['data']['customer']['defaults']['payment_method_id'],
                    "billing_address_id" => $customer_response['data']['data']['customer']['defaults']['billing_address_id'],
                    "shipping_address_id" => $customer_response['data']['data']['customer']['defaults']['shipping_address_id'],
                  ]
                ]
            ];
            $subscription_response = $this->requestService->createSubscription($subscription);
            if ($subscription_response["status"] != "success") {
              $this->requestService->refund([
                'transaction_id' => $transaction['data']['id'],
                'amount' => $transaction['data']['amount']
              ]);
              give_set_error('FluidPay Error',
                __('Not able to create a subscription on FluidPay gateway , please contact our support team', 'give-recurring'));
              give_send_back_to_checkout('?payment-mode=fluidpay');
            }
          } else {
            give_set_error('FluidPay Error',
              __('Not able to create a Transaction on FluidPay gateway , please contact our support team', 'give-recurring'));
            give_send_back_to_checkout('?payment-mode=fluidpay');
          }
        } else {
          give_set_error('FluidPay Error',
            __('Not able to create a Transaction on FluidPay gateway , please contact our support team', 'give-recurring'));
          give_send_back_to_checkout('?payment-mode=fluidpay');
        }

        do_action('give_recurring_pre_create_payment_profiles', $this);
        // Create subscription payment profiles in the gateway.
        $this->create_payment_profiles();
        do_action('give_recurring_post_create_payment_profiles', $this);

        // Look for errors after trying to create payment profiles.
        $errors = give_get_errors();
        if ($errors) {
          give_send_back_to_checkout('?payment-mode=fluidpay');
        }
        // Record the subscriptions and finish up.
        $this->record_signup();
        // Finish the signup process.
        if ($subscription_response["status"] == "success") {
          $give_subscription = give_recurring_get_subscription_by('payment', $this->payment_id);
          $give_subscription->update([
            'profile_id' => $subscription_response['data']['id']
          ]);
          //link customer id to subscription so when a webhook received with a customer ID we will know that a new donation has been occurred
          $subscription_to_customer = get_option('subscription_to_customer_' . $subscription_response['data']['customer']['id']);
          if (!$subscription_to_customer) {
            add_option('subscription_to_customer_' . $subscription_response['data']['customer']['id'], $give_subscription->id);
          }

        }

        // Finish the signup process.
        // Gateways can perform off-site redirects here if necessary.
        $this->complete_signup();

        wp_redirect(give_get_success_page_uri());
      } else {
        give_set_error('FluidPay Error',
          __($customer_response['message'], 'give-recurring'));
        give_send_back_to_checkout('?payment-mode=fluidpay');
      }
      // Look for any last errors.
      $errors = give_get_errors();
      // We shouldn't usually get here, but just in case a new error was recorded,
      // we need to check for it.
      if ($errors) {
        give_send_back_to_checkout('?payment-mode=fluidpay');
      }
    }
  }

  /**
   * Get price id
   *
   * @return string
   * @since  1.6.2
   * @access private
   */
  private function get_price_id(): string
  {
    return array_key_exists('give-price-id', $this->purchase_data['post_data']) ?
      $this->purchase_data['post_data']['give-price-id'] : '';
  }

  /**
   * This function is used to display billing details only when enabled.
   *
   * @param $form_id
   * @return void
   * @since 1.0.0
   */
  public function display_billing_details($form_id)
  {
    // Remove Address Fields if user has option enabled.
    if (!give_get_option('give_option_fluidpay_collect_billing_details')) {
      remove_action('give_after_cc_fields', 'give_default_cc_address_fields');
    }
    // Ensure CC field is in place properly.
    do_action('give_cc_form', $form_id);
  }
}

new Give_Recurring_FluidPay();
