=== Give - FluidPay Gateway ===
Contributors: givewp
Tags: donations, donation, ecommerce, e-commerce, fundraising, fundraiser, fluidpay, gateway
Requires at least: 4.9
Tested up to: 5.8
Stable tag: 1.0.0
Requires Give: 2.16.0
License: GPLv3
License URI: https://opensource.org/licenses/GPL-3.0

FluidPay Gateway Add-on for Give

== Description ==

This plugin requires the GiveWP Core plugin activated to function properly. When activated, it adds a payment gateway for fluidpay.com.

== Installation ==

= Minimum Requirements =

* WordPress 4.9 or greater
* PHP version 7.4 or greater
* MySQL version 5.6 or greater