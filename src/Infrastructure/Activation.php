<?php
namespace GiveFluidPay\Infrastructure;


/**
 * Class responsible for registering and handling add-on activation hooks.
 *
 * @package     GiveRecurring
 * @copyright   Copyright (c) 2020, GiveWP
 */
class Activation {
	/**
	 * Activate add-on action hook.
	 *
	 * @since 2.4.0
	 * @return void
	 */
	public static function activateAddon() {

	}

	/**
	 * Deactivate add-on action hook.
	 *
	 * @since 2.4.0
	 * @return void
	 */
	public static function deactivateAddon() {}

	/**
	 * Uninstall add-on action hook.
	 *
	 * @since 2.4.0
	 * @return void
	 */
	public static function uninstallAddon() {}
}
