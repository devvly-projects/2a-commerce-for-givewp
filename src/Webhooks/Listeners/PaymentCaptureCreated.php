<?php

namespace GiveFluidPay\Webhooks\Listeners;

use GiveRecurring\Webhooks\Contracts\EventListener;
use Give_Subscription;
use GiveRecurring\Infrastructure\Log;

/**
 * Class PaymentCaptureCreated
 * @package GiveFluidPay\Webhooks\Listeners
 *
 * @since 1.12.6
 */
class PaymentCaptureCreated implements EventListener
{

  function __construct() {
  }

    /**
     * @inheritDoc
     *
     * @return void
     */
    public function processEvent($event)
    {
      $transactionId = $event["data"]["response_body"]["card"]["id"] ?? '';
      // Must have the transaction id.
      if (empty($transactionId)) {
        return;
      }
      $customerId = $event["data"]["customer_id"] ?? '';
      if (empty($customerId)) {
        return;
      }
      $subscriptionId = get_option('subscription_to_customer_' . $customerId);
      if (!$subscriptionId) {
        Log::error(
          'FluidPay Webhook Error',
          [
            'Description' => 'The Recurring FluidPay gateway could not find the subscription from the webhook data.',
            'Event Data' => $event,
            'Transaction Data' => $event
          ]
        );
        return;
      }
      $subscription = new Give_Subscription($subscriptionId);
      if(0 === $subscription->id){
        Log::error(
          'FluidPay Webhook Error',
          [
            'Description' => 'The Recurring FluidPay gateway could not find the subscription from the webhook data.',
            'Event Data' => $event,
            'Transaction Data' => $event
          ]
        );
        return;
      }

      if('sale' == $event['data']['type']) {
        $total_payments = intval($subscription->get_total_payments());
        $bill_times = $subscription->bill_times;
        if (
          0 === $bill_times // Ongoing
          ||
          $total_payments < $bill_times // Bills amount limit not exceeded
        ) {
          $args = [
            'amount' => $event['data']['base_amount'] / 100,
            'transaction_id' => $transactionId,
            'gateway' => $subscription->gateway
          ];
          // We have a renewal.
          $subscription->add_payment($args);
          $subscription->renew();
        }
      }
    }

}