<?php

namespace GiveFluidPay\Webhooks\Listeners;

use Give\PaymentGateways\PayPalCommerce\Webhooks\Listeners\EventListener;
use Give_Subscription;

class CustomerSubscriptionCreated implements EventListener
{
   public function __construct()
   {

   }
    public function processEvent($event)
    {

        $subscription = $this->getSubscription($event);
        if (!$this->isSubscriptionProcessable($subscription->id, $subscription->status)) {
            return;
        }
        give_recurring_update_subscription_status($subscription->id, 'active');
        if( 'pending' ===  get_post_status( $subscription->parent_payment_id ) ) {
            give_update_payment_status($subscription->parent_payment_id, 'processing');
        }
    }

    private function getSubscription($event)
    {
        $subscription = new Give_Subscription(
            $event['data']['subscription_profile_id'],
            true
        );
        if (!$subscription->id) {
            $donationMetaData = $event->data->object->metadata;
            $donationId = !empty($donationMetaData['Donation Post ID']) ? $donationMetaData['Donation Post ID'] : 0;
            $subscription = give_recurring_get_subscription_by( 'payment', $donationId );
        }
        return $subscription;
    }

    /**
     * @since 1.15.0
     *
     * @param int $subscriptionId
     * @param string $subscriptionStatus
     *
     * @return bool
     */
    private function isSubscriptionProcessable($subscriptionId, $subscriptionStatus)
    {
        return $subscriptionId && !in_array($subscriptionStatus, ['active', 'cancelled', 'completed', 'expired']);
    }


}
