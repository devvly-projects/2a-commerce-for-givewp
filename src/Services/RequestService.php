<?php

namespace GiveFluidPay\Services;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonException;
use Psr\Http\Message\ResponseInterface;

class RequestService
{
  protected string $api_base_url;

  protected string $api_key;

  protected \GuzzleHttp\ClientInterface $client;

  /**
   * @param $api_url string Base API URL
   * @param $key string Distributor secret key
   * @param ClientInterface|null $testing_client Custom GuzzleHttp client for testing purposes
   */
  public function __construct(string $api_url, string $key, ClientInterface $testing_client = null)
  {
    $this->api_base_url = $api_url;
    $this->api_key = $key;
    $this->client = new Client([
      'base_uri' => $this->api_base_url,
      'verify' => false,
      'headers' => [
        'Authorization' => $this->api_key
      ]
    ]);

    if ($testing_client) {
      $this->client = $testing_client;
    }
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function getAPIKeys() {
    $data = [];

    /** @var ResponseInterface $response */
    $response = $this->client->get('user/apikeys');
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function createCustomer(array $customer) {
    $data = [];

    /** @var ResponseInterface $response */
    $response = $this->client->post('vault/customer', [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($customer)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function getAllPlans() {
    $data = [];

    /** @var ResponseInterface $response */
    $response = $this->client->get('recurring/plans');
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;

  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function processTransaction($transaction)
  {
    $data = [];

    /** @var ResponseInterface $response */
    $response = $this->client->post('transaction', [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($transaction)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function refund($refund_data)
  {
    $data = [];

    /** @var ResponseInterface $response */
    $response = $this->client->post("transaction/{$refund_data['transaction_id']}/refund", [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($refund_data)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function createPlan($plan) {
    $data = [];

    /** @var ResponseInterface $response */
    $response = $this->client->post('recurring/plan', [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($plan)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function createSubscription($subscription) {
    $data = [];

    /** @var ResponseInterface $response */
    $response = $this->client->post('recurring/subscription', [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'body' => json_encode($subscription)
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }

  /**
   * @return array|mixed
   * @throws GuzzleException
   * @throws JsonException
   */
  public function cancelSubscription($subscription_id) {
    $data = [];

    /** @var ResponseInterface $response */
    $response = $this->client->delete("recurring/subscription/$subscription_id", [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json'
      ],
      'query'
    ]);
    if (200 == $response->getStatusCode()) {
      $contents = $response->getBody()->getContents();
      $data = json_decode($contents, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return $data;
  }
}