<?php

namespace GiveFluidPay;

trait RecurringPaymentTrait {
  public function buildPaymentData($donation_amount, &$donation_data): array
  {
    return [
      'price' => $donation_amount,
      'give_form_title' => $donation_data['post_data']['give-form-title'],
      'give_form_id' => intval($donation_data['post_data']['give-form-id']),
      'give_price_id' => $donation_data['post_data']['give-price-id'] ?? '',
      'date' => $donation_data['date'],
      'user_email' => $donation_data['user_email'],
      'purchase_key' => $donation_data['purchase_key'],
      'currency' => give_get_currency($donation_data['post_data']['give-form-id'], $donation_data),
      'user_info' => $donation_data['user_info'],
      'status' => 'pending'
    ];
  }

  public function buildCustomerData(&$donation_data): array
  {
    return [
      'id_format' => 'xid_type_last4',
      'description' => 'test description',
      'flags' => ['surcharge_exempt'],
      'donation_id' => (string)$this->payment_id,
      'default_payment' => [
        'card' => [
          'number' => $this->purchase_data['card_info']['card_number'],
          'expiration_date' => $this->purchase_data['card_info']['card_exp_month'] . '/' . substr($this->purchase_data['card_info']['card_exp_year'], -2),
          'cvc' => $this->purchase_data['card_info']['card_cvc']
        ]
      ],

      'default_billing_address' => [
        'first_name' => $donation_data['user_info']['first_name'],
        'last_name' => $donation_data['user_info']['last_name'],
        'company' => 'company_name',
        'address_line_1' => $donation_data['user_info']['address']['line1'],
        'address_line_2' => '',
        'city' => $donation_data['user_info']['address']['city'],
        'state' => $donation_data['user_info']['address']['state'],
        'postal_code' => $donation_data['user_info']['address']['zip'],
        'country' => 'US',
        'email' => $donation_data['user_email']
      ],
      'default_shipping_address' => [
        'first_name' => $donation_data['user_info']['first_name'],
        'last_name' => $donation_data['user_info']['last_name'],
        'company' => 'company_name',
        'address_line_1' => $donation_data['user_info']['address']['line1'],
        'address_line_2' => '',
        'city' => $donation_data['user_info']['address']['city'],
        'state' => $donation_data['user_info']['address']['state'],
        'postal_code' => $donation_data['user_info']['address']['zip'],
        'country' => 'US',
        'email' => $donation_data['user_email']
      ]
    ];
  }

  public function buildRecurringTransactionRequest(&$customer_response, &$donation_data): array
  {
    return [
      'type' => 'sale',
      'billing_method' => 'recurring',
      'order_id' => $this->payment_id,
      'amount' => $donation_data['price'] * 100,
      'tax_amount' => 0,
      'shipping_amount' => 0,
      'currency' => 'USD',
      'description' => 'test transaction',
      'ip_address' => $_SERVER['HTTP_CF_CONNECTING_IP'] ?? $this->getRealIpAddr(),
      'email_receipt' => FALSE,
      'email_address' => $donation_data['user_email'],
      'create_vault_record' => FALSE,
      'donation_id' => (string)$this->payment_id,
      'test_field' => 'qwqwqqwq',
      'custom_fields' => [
        $customer_response['data']['id'] => ['Donation payment'],
      ],
      'payment_method' => [
        'customer' => [
          'id' => $customer_response['data']['id'],
          'payment_method_type' => 'card',
          'payment_method_id' => $customer_response['data']['data']['customer']['defaults']['payment_method_id'],
          'billing_address_id' => $customer_response['data']['data']['customer']['defaults']['billing_address_id'],
          'shipping_address_id' => $customer_response['data']['data']['customer']['defaults']['shipping_address_id'],
          'custom_fields' => [$customer_response['data']['id'] => ['test_field']],
        ]
      ],
      'billing_address' => [
        'first_name' => $donation_data['user_info']['first_name'],
        'last_name' => $donation_data['user_info']['last_name'],
        'company' => 'company_name',
        'address_line_1' => $donation_data['user_info']['address']['line1'],
        'address_line_2' => '',
        'city' => $donation_data['user_info']['address']['city'],
        'state' => $donation_data['user_info']['address']['state'],
        'postal_code' => $donation_data['user_info']['address']['zip'],
        'country' => 'US',
        'email' => $donation_data['user_email']
      ]
    ];
  }
}