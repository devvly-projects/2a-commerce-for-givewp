<?php

namespace GiveFluidPay;

trait HelperTrait {
  /**
   * @param string $period
   * @return string
   */
  public function getBillingFrequency(string $period = ''): string {
    return ('month' == $period) ? 'monthly' : 'daily';
  }

  public function getBillingDays($billing_cycle_interval, $billing_frequency): ?string
  {
    $billing_days = '';
    if ($billing_frequency == 'daily') {
      /*for ($i=1;$i<=30;$i+=$billing_cycle_interval){
        $billing_days=$billing_days.$i.",";
      }
      $last_character=substr($billing_days, -1);
      if($last_character==","){
        $billing_days=substr_replace($billing_days ,"", -1);
      }*/
      return (string)$billing_cycle_interval;
    }

    if ($billing_frequency == 'monthly') {
      return '0';
    }
    return null;
  }

  public function getRealIpAddr()
  {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
  }

  public function getNextBillingDate($billing_cycle_interval, $billing_frequency)
  {
    if ($billing_frequency == 'daily') {
      return date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $billing_cycle_interval . ' days'));
    }
    if ($billing_frequency == 'monthly') {
      return date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $billing_cycle_interval . ' months'));
    }
    return null;
  }
}