<?php

namespace GiveFluidPay\Donations\ServiceProviders;

use Give\Helpers\Hooks;
use GiveFluidPay\Donations\Controllers\DonationsController;

/**
 * Class ServiceProvider
 * @package GiveFluidPay\Donations\ServiceProviders
 *
 * @since 1.12.6
 */
class ServiceProvider implements \Give\ServiceProviders\ServiceProvider {


  /**
   * @inheritDoc
   */
  public function register() {
  }

  /**
   * @inheritDoc
   */
  public function boot() {
    Hooks::addAction( 'wp_ajax_give_fluidpay_control_donations', DonationsController::class, 'handle' );
  }

}