<?php

namespace GiveFluidPay\Donations\Controllers;

use GiveFluidPay\Donations\Repositories\DonationsRepository;

/**
 * Class DonationsController
 * @package GiveFluidPay\Donations\Controllers
 * @since 2.3.0
 */
class DonationsController {
  /**
   * @var DonationsRepository
   */
  private $donationsRepository;


  /**
   * AchLinkTokenController constructor.
   *
   * @param  DonationsRepository $donationsRepository
   */
  public function __construct(
    DonationsRepository $donationsRepository
  ) {
    $this->donationsRepository = $donationsRepository;
  }
  public function handle() {

  }

}
